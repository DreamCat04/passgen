package ch.bbw.passGen;

public class PassGen {
	public PassGen() {
		String chars = "abcdefghijklmnopqrstuvqxyz0123456789#*%&7(.ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String out = "";
		int rand = 0;
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 4; j++) {
				rand = (int) Math.round(Math.random() * 68);
				out += chars.charAt(rand);
			}
			out += "-";
		}
		out = out.substring(0, out.length() - 1);
		System.out.println(out);
	}
}
